
# Voting

A voting is a collection of votes for a votable element. A `module_item` can be a votable element if it is of the right type (`module_item_type`):

**Votable module_item_types**

  - AMENDMENT
  - CITIZEN_INITIATIVE
  - ~~COLLEGE_DECISION~~
  - ~~COMMISSION_LETTER~~
  - ~~COUNCIL_MEETING~~
  - ~~COUNCIL_NOTICE~~
  - COUNCIL_PROPOSAL
  - ~~DECISION_LIST~~
  - ~~DISCUSSION_PAPER~~
  - INITIATIVE_PROPOSAL
  - ~~LETTER~~
  - ~~LIST_OF_INCOMING_ITEMS~~
  - MOTION
  - ~~REPORT~~
  - ~~WRITTEN_QUESTION~~

## Voting types
A voting can be official, or community based. Each votable item has at most 1 official voting and a configurable number of community votings.

### Offical voting
The official voting are the votes cast by the members of the gremium that have officially cast their vote. This data comes from the official public records registry.

### Community voting
A community voting are the votes cast by users in the system.

## Workflow
### Official voting workflow
The voting records for the official votes (by the members of the gremium) are synchronized automatically using the `NotubizVoteService`.

Worker (Cron)

  - Fetch all module_items without voting item
  -
